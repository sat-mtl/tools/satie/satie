SatieIntrospection {
	var context;
	var <allPlugins;
	var spatList;

	*new {|satieContext|
		if (satieContext.class == Satie,
			{
				^super.newCopyArgs(satieContext);
			},
			{
				"ERROR: Wrong argument, should be Satie".error;
				^"null";
			}
		)
	}

	/* *****
	*  SATIE system level queries
	*
	*/

	getCurrentConfig{
		var ret = Dictionary.new();
		var varNames = context.config.class.instVarNames[0..6];
		varNames.do{ | v |
			if (v != "server".asSymbol, // skip the server because we cannot get valuable information from it anyways
				{ret.add(v.asSymbol -> context.config.slotAt(context.config.slotIndex(v.asSymbol)))},
				{ret.add(v.asSymbol -> "NULL".asSymbol)}
			)
		};
		^ret;
	}

	currentConfigJSON {
		^SatieJson.stringify(this.getCurrentConfig);
	}

	/* *****
	*	queries about uninstantiated plugin sources
	*
	*/
	updatePluginsList{
		allPlugins = context.config.generators.merge(context.config.effects).merge(context.config.postprocessors).merge(context.config.hoa).merge(context.config.spatializers).merge(context.config.monitoring);
	}

	// return a dictionary audio plugins. Key is the type of plugin, value a Set of names.
	getPluginList {
		var ret = Dictionary.new();
		ret.add(\generators -> context.config.generators.keys);
		ret.add(\effects -> context.config.effects.keys);
		ret.add(\mastering -> context.config.postprocessors.keys);
		ret.add(\ambiPostProcessors -> context.config.hoa)
		^ret;
	}

	pluginListJSON {
		^SatieJson.stringify(this.getPluginList);
	}

	// @plugin
	getPluginArguments { | plugin |
		var argnames, plugs;
		this.updatePluginsList;
 		allPlugins.keysDo({|key|
			if(key === plugin.asSymbol,
				{

					^argnames = allPlugins[key].function.def.keyValuePairsFromArgs;
				},
				{
					if(context.debug,
						{"% tried % in % and found none...\n".format(this.class.getBackTrace, plugin, allPlugins).warn}
					);
					argnames = "null";
				}
			);
		});
		^argnames;
	}

	getPluginDescription { | plugin |
		//  plugin: symbol - synthdef name
		var description;
		this.updatePluginsList;
		allPlugins.keysDo({|key|
			if(key === plugin.asSymbol,
				{
					^description = allPlugins[key.asSymbol].description;
				},
				{
					if(context.debug,
						{"% tried % in % and found none...".format(this.class.getBackTrace, plugin, allPlugins).warn}
					);
					description = "null";
				}
			);
		});
		^description;
	}

	getPluginInfo { | plugin |
		//  plugin: symbol - synthdef name
		var description, arguments, dico;
		dico = Dictionary.new();
		description = this.getPluginDescription(plugin);
		arguments = this.getPluginArguments(plugin);
		dico.add(\description -> description);
		dico.add(\arguments -> arguments.as(Dictionary));
		^dico;
	}

	getPluginInfoJSON { | plugin |
		var dico = Dictionary.new();
		dico.add(plugin.asSymbol -> this.getPluginInfo(plugin));
		^SatieJson.stringify(dico);
	}

	// we will probably want to get other available fields of a plugin, we can list them here
	getPluginFields { | plugin |
		var fields, plugClass, plugInstance, ret;
		fields = Dictionary.new();
		ret = Dictionary.new();
		this.updatePluginsList;
		allPlugins.keysDo {| key |
			if(key === plugin.asSymbol,
				{
					plugInstance = allPlugins[key.asSymbol];
					plugClass = plugInstance.class;
					plugClass.instVarNames.do({|item, i|
						fields.add(item.asSymbol -> plugInstance.instVarAt(i).asCompileString);
					});
				},
				{
					if(context.debug,
						{"% tried % in % and found none...".format(this.class.getBackTrace, plugin, allPlugins).warn}
					);
				}
			)
		};
		ret.add(plugin.asSymbol -> fields);
		^ret;
	}

	getPluginFieldsJSON {|spatPlug|
		^SatieJson.stringify(this.getPluginFields(spatPlug.asSymbol));
	}

	getSpatializerArguments {| spatPlug |
		var argnames;
		this.updateSpatList();
		if(spatList.keys.includes(spatPlug.asSymbol),
			{
				^argnames = spatList[spatPlug.asSymbol].function.def.keyValuePairsFromArgs;
			},
			{
				if(context.debug,
					{"% tried % in % and found none...\n".format(this.class.getBackTrace, spatPlug, spatList).warn}
				);
				argnames = "null";
			}
		);
		^argnames;
	}

	updateSpatList {
		spatList = context.config.spatializers;
	}

	/* *****
	*	queries about compiled synths & effects
	*
	*/

	/*
		Return a dictionary of instances of generators
		key: ID (given name)
		value: source name
	*/
	getGenerators {
		^context.generators;
	}

	getEffects {
		^context.effects;
	}

	getPostProcessors {
		^context.mastering;
	}

	getAmbiPostProcessors {
		^context.ambiPostProcessors;
	}

	// grouped by generators and effects
	getSynthDefs {
		var instances = Dictionary.new();
		instances.add(\generators -> this.getGenerators());
		instances.add(\effects -> this.getEffects());
		instances.add(\mastering -> this.getPostProcessors());
		^instances;
	}

	getSynthDefsJSON {
		^SatieJson.stringify(this.getSynthDefs);
	}

	getPluginsSrc {
		var ret;
		ret = Dictionary.new;
		ret.add(\generators -> Dictionary.new);
		context.config.generators.do({ | item, i |
			ret[\generators.asSymbol].add(item.name.asSymbol -> Dictionary.newFrom([\description, item.description, \type, item.name]));
		});
		ret.add(\generators -> Dictionary.new);
		context.config.generators.do({ | item, i |
			ret[\generators.asSymbol].add(item.name.asSymbol -> Dictionary.newFrom([\description, item.description, \type, item.name]));
		});

		ret.add(\effects -> Dictionary.new);
		context.config.effects.do({ | item, i |
			ret[\effects].add(item.name.asSymbol -> Dictionary.newFrom([\description, item.description, \type, item.name]));
		});
		ret.add(\spatializers -> Dictionary.new);
		context.config.spatializers.do({ | item, i |
			ret[\spatializers].add(item.name.asSymbol -> Dictionary.newFrom([\description, item.description, \type, item.name]));
		});
		ret.add(\hoa -> Dictionary.new);
		context.config.hoa.do({ | item, i |
			ret[\hoa].add(item.name.asSymbol -> Dictionary.newFrom([\description, item.description, \type, item.name]));
		});
		ret.add(\mappers -> Dictionary.new);
		context.config.mappers.do({ | item, i |
			ret[\mappers].add(item.name.asSymbol -> Dictionary.newFrom([\description, item.description, \type, item.name]));
		});
		ret.add(\postprocessors -> Dictionary.new);
		context.config.postprocessors.do({ | item, i |
			ret[\postprocessors].add(item.name.asSymbol -> Dictionary.newFrom([\description, item.description, \type, item.name]));
		});
		ret.add(\monitoring -> Dictionary.new);
		context.config.monitoring.do({ | item, i |
			ret[\monitoring].add(item.name.asSymbol -> Dictionary.newFrom([\description, item.description, \type, item.name]));
		});
		^ret;
	}

	getPluginsSrcJSON{
		^SatieJson.stringify(this.getPluginsSrc());
	}

	getSynthDefInfo { | synthName |
		var srcName, description, arguments, ret;
		description = Dictionary.new();
		arguments = Dictionary.new();
		"Deprecation warning: this method may be phased out with time. Please use getSynthDefParameters (or /satie/plugindetails via OSC)".warn;
		this.getSynthDefs.keysValuesDo({| category, instances |
			instances.keysValuesDo({| name, srcName |
				if (synthName.asSymbol == name.asSymbol,
					{
						var plug = this.getPluginInfo(srcName.asSymbol);
						ret = Dictionary.new();
						description = plug[\description];
						arguments = plug[\arguments];
						srcName = srcName.asSymbol;
						ret.add(synthName.asSymbol -> Dictionary.with(*[
							\srcName -> srcName,
							\description -> description,
							\arguments -> arguments])
						);
						^ret;
					},
					{
						if (context.debug,
							{"% did not find % in %".format(this.class.getBackTrace, synthName, instances).postln});
						ret = "null";
					}
				);
			});
		});
		^ret;
	}

	getSynthDefInfoJSON { | id |
		^SatieJson.stringify(this.getSynthDefInfo(id));
	}

	getSynthDefParameters { | synthName |
		var srcName, description, arguments, ret;
		description = Dictionary.new();
		arguments = Array.new();

		this.getSynthDefs.keysValuesDo({| category, instances |
			instances.keysValuesDo({| name, srcName |
				if (synthName.asSymbol == name.asSymbol,
					{
						var plug = this.getPluginInfo(srcName.asSymbol);
						ret = Dictionary.new();
						description = plug[\description];
						arguments = this.buildArgStruct(plug[\arguments]);
						srcName = srcName.asSymbol;
						ret.add(synthName.asSymbol -> Dictionary.with(*[
							\srcName -> srcName,
							\description -> description,
							\arguments -> arguments])
						);
						^ret;
					},
					{
						if (context.debug,
							{"% did not find % in %".format(this.class.getBackTrace, synthName, instances).postln});
						ret = "null";
					}
				);
			});
		});
		^ret;
	}

	getSynthDefParametersJSON{ | id |
		^SatieJson.stringify(this.getSynthDefParameters(id));
	}

	buildArgStruct { | argDico |
		var ret, dico;
		ret = Array.new();
		argDico.keysDo({ | key|
			dico = Dictionary.new();
			dico.add(\name -> key);
			dico.add(\value -> this.checkForNil(argDico[key]));
			dico.add(\type -> argDico[key].class.asString);
			ret = ret.add(dico);
		});
		^ret;
	}

	checkForNil {|val|
		var ret;
		if (val.notNil,
			{
				ret = val;
			},
			{
				ret = "unused".quote;
			}
		);
		^ret;
	}

	/* *****
	*	queries about SynthDefs and running Synths
	*
	*/

	/*
		Return a dictionary of instances of generators
		key: ID (given name)
		value: source name
	*/

	getValue { | instanceName, control, group=\default |
		var ret;
		ret = Dictionary.new();
		context.groupInstances[group.asSymbol][instanceName.asSymbol].get(control.asSymbol, { | value |
			ret.add(control.asSymbol -> value);
			postf("(Dictionary asynchronously populated)\n% : %%, %\n", instanceName, "\\", control, value);
		});
		^ret;
	}

	getDefaultSynthParameters { | instanceName, group=\default |
		var ret, definition, parameterDictionary, i=0;
		ret = Dictionary.new();
		definition = context.groupInstances[group.asSymbol][instanceName.asSymbol].defName;
		parameterDictionary = this.getSynthDefParameters(definition);
		while ( { parameterDictionary[definition.asSymbol][\arguments][i] != nil }, {
			ret.add(parameterDictionary[definition.asSymbol][\arguments][i][\name].asSymbol -> parameterDictionary[definition.asSymbol][\arguments][i][\value]);
			i = i + 1;
		});
		^ret;
	}

	getDefaultSynthParametersJSON{ | id |
		^SatieJson.stringify(this.getDefaultSynthParameters(id));
	}

	getSynthParameters { | instanceName, group=\default |
		var ret, definition, parameterDictionary, i=0;
		ret = Dictionary.new();
		definition = context.groupInstances[group.asSymbol][instanceName.asSymbol].defName;
		parameterDictionary = this.getSynthDefParameters(definition);
		ret.add(instanceName.asSymbol -> Array.new(parameterDictionary[definition.asSymbol][\arguments].size));
		while ( { parameterDictionary[definition.asSymbol][\arguments][i] != nil }, {
			ret.at(instanceName.asSymbol).add(parameterDictionary[definition.asSymbol][\arguments][i][\name].asSymbol);
			i = i + 1;
		});
		^ret;
	}

	getSynthParametersJSON{ | id |
		^SatieJson.stringify(this.getSynthParameters(id));
	}

	getAllValues { | instanceName, group=\default |
		var ret, definition, parameterDictionary, i=0;
		ret = Dictionary.new();
		definition = context.groupInstances[group.asSymbol][instanceName.asSymbol].defName;
		parameterDictionary = this.getSynthDefParameters(definition);
		postf("(Dictionary asynchronously populated) \n");
		while ( { parameterDictionary[definition.asSymbol][\arguments][i] != nil }, {
			var argument = parameterDictionary[definition.asSymbol][\arguments][i][\name].asSymbol;
			context.groupInstances[group.asSymbol][instanceName.asSymbol].get( argument, { | value |
				ret.add(argument -> value);
				postf("%%, %, ", "\\", argument, value);
			});
			i = i + 1;
		});
		^ret;
	}

	getSynthValues { | instanceName, returnFunction |
		var resp, ret, mapID, done = false;
		ret = Dictionary.new;
		mapID = Dictionary.new;
		context.groupInstances.keys.do({|item|
			context.groupInstances[item].keys.do({|subitem|
				mapID.add( context.groupInstances[item][subitem].nodeID -> subitem);
			})
		});
		resp = OSCFunc({ |msg|
			msg.do( { | item, i |
				if( item == -1 && mapID[msg[i-1]].asSymbol == instanceName,{
					ret.add(mapID[msg[i-1]].asSymbol -> Dictionary.newFrom( Array.fill(msg[i+2], { | j | msg[i+3+j] } ) ) );
				})
			});
			Post << "\n" << ret << Char.nl;
			returnFunction.value(ret);
			done = true;
		}, '/g_queryTree.reply').oneShot;

		context.config.server.sendMsg("/g_queryTree", 0, true.binaryValue);
		SystemClock.sched(3, {
			if(done.not) {
				resp.free;
				"Server failed to respond to the information query!".warn;
			};
		});
		^ret;
	}

	getAllSynths { | returnFunction |
		var resp, ret, mapID, done = false;
		ret = Dictionary.new;
		mapID = Dictionary.new;
		context.groupInstances.keys.do({|item|
			context.groupInstances[item].keys.do({|subitem|
				mapID.add( context.groupInstances[item][subitem].nodeID -> subitem);
			})
		});
		resp = OSCFunc({ |msg|
			msg.do( { | item, i |
				if( item == -1,{
					ret.add(mapID[msg[i-1]].asSymbol -> Dictionary.newFrom( Array.fill(msg[i+2], { | j | msg[i+3+j] } ) ) );
				})
			});
			Post << "\n" << ret << Char.nl;
			returnFunction.value(ret);
			done = true;
		}, '/g_queryTree.reply').oneShot;

		context.config.server.sendMsg("/g_queryTree", 0, true.binaryValue);
		SystemClock.sched(3, {
			if(done.not) {
				resp.free;
				"Server failed to respond to the information query!".warn;
			};
		});
		^ret;
	}

	getDefValue { | control, instanceName, group=\default |
		var ret, definition, parameterDictionary, i=0;
		ret = Dictionary.new();
		//parameterDictionary = this.getDefaultSynthParameters(context.groupInstances[group.asSymbol][instanceName.asSymbol]);
		parameterDictionary = this.getDefaultSynthParameters(instanceName.asSymbol);
		ret.add(control -> parameterDictionary[control.asSymbol]);
		^ret;
	}

	getSpatializerSpeakerAngles { | spatializerName |
		var ret;
		ret = Dictionary.new();

		case
			{context.config.spatializers[spatializerName.asSymbol].angles.isKindOf(Array)}
				{
					ret.add(spatializerName.asSymbol -> context.config.spatializers[spatializerName.asSymbol].angles)
				}
			{context.config.spatializers[spatializerName.asSymbol].angles.isKindOf(VBAPSpeakerArray)}
				{
					ret.add(spatializerName.asSymbol -> Array.new(context.config.spatializers[spatializerName.asSymbol].angles.numSpeakers));
					for (0, context.config.spatializers[spatializerName.asSymbol].angles.numSpeakers - 1,
						{
							| i |
							ret[spatializerName.asSymbol].add([
								context.config.spatializers[spatializerName.asSymbol].angles.speakers[i].azi,
								context.config.spatializers[spatializerName.asSymbol].angles.speakers[i].ele
							]);
						}
					)
				};
		^ret;
	}

	getSpeakerAngles {
		var ret, currentListeningFormat;
		currentListeningFormat = this.getCurrentConfig[\listeningFormat][0];
		currentListeningFormat.postln;
		ret = this.getSpatializerSpeakerAngles(currentListeningFormat.asSymbol);
		^ret;
	}

	getSpeakerAnglesJSON{ | id |
		^SatieJson.stringify(this.getSpeakerAngles(id));
	}

	checkSynthParameter{ | instanceName, parameter |
		var ret, parList;
		parList = this.getSynthParameters(instanceName.asSymbol);
		parList[instanceName.asSymbol].do({ | item, i |
			ret=false;
			if (item === ~parameter.asSymbol) {ret = true};
		});
		^ret;
	}
}
