// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

SatieFactory {

	classvar <lebedev50buffer;

	*loadLebedev50Buffer { |server|

		if(server.isNil) { Error("% the server argument is Nil".format(thisMethod)).throw };
		if(server.serverRunning.not) { Error("% cannot load Buffer. Server is not booted".format(thisMethod)).throw };

		lebedev50buffer = Buffer.loadCollection(server, SatieLebedev50.vbapBufferData);
		^lebedev50buffer
	}

	*encoderUGenGraph { |type = \wave|
		var hoaEncoder;

		hoaEncoder = switch(type)
			{ \harmonic } { this.harmonicEncoder }
			{ \wave } { this.waveEncoder }
			{ \lebedev50 } { this.lebedev50Encoder }
			{
				"hoaEncoderType must be either \\harmonic, \\wave, or \\lebedev50".warn;
				Error("Invalid value -> \'%\'".format(type)).throw;
			};

		^{ |in, order = 1, aziDeg = 0, eleDeg = 45, gainDB = -99, delayMs = 1,
			lpHz = 15000, hpHz = 5, spread = 0.01, plane_spherical = 0, radius = 2, speaker_radius = 1.07|

			var gain = gainDB.dbamp;      // convert gainDB to gainAMP
			var delay = delayMs * 0.001;  // convert to seconds
			var slewDelay = 0.3;          // note: this needs to be improved ... smoother
			var slewGain = 0.1;
			var slewFilter = 0.3;
			var slewPanning = 0.03;
			var outsig;

			// limit cutoff freq range and smooth changes
			lpHz = lpHz.clip(5.0, 20000.0).lag3(slewFilter);
			hpHz = hpHz.clip(5.0, 20000.0).lag3(slewFilter);

			outsig = in * gain.lag(slewGain);
			outsig = DelayC.ar(
				outsig,
				maxdelaytime: 0.5,
				delaytime: delay.lag(slewDelay)
			);
			outsig = LPF.ar(outsig, lpHz);
			outsig = BHiPass.ar(outsig, hpHz);

			SynthDef.wrap(hoaEncoder, prependArgs: [outsig, order, aziDeg, eleDeg, spread, plane_spherical, radius, speaker_radius, slewPanning])
		}
	}

	*lebedev50Encoder {
		^{ |outsig, order, aziDeg, eleDeg, spread, plane_spherical, radius, speaker_radius, slewPanning|
			aziDeg = aziDeg.circleRamp(slewPanning);
			eleDeg = eleDeg.circleRamp(slewPanning);
			outsig = VBAP.ar(
				numChans: 50,
				in: outsig,
				bufnum: lebedev50buffer,
				azimuth: aziDeg,
				elevation: eleDeg,
				spread: spread.linlin(0.0, 1.0, 20.0, 100.0)
			);
			HOAEncLebedev50.ar(order, outsig, filters: 0)
		}
	}

	*harmonicEncoder {
		^{ |outsig, order, aziDeg, eleDeg, spread, plane_spherical, radius, speaker_radius, slewPanning|
			var az = aziDeg.degrad;
			var el = eleDeg.degrad;
			var harmonics;

			harmonics = HOASphericalHarmonics.coefN3D(
				order,
				CircleRamp.kr(az, slewPanning, -1pi, 1pi),
				CircleRamp.kr(el, slewPanning, -1pi, 1pi),
			);
			// b-format by way of spherical harmonics multiplication
			outsig * harmonics
		}
	}

	*waveEncoder {
		^{ |outsig, order, aziDeg, eleDeg, spread, plane_spherical, radius, speaker_radius, slewPanning|
			var az = aziDeg.degrad;
			var el = eleDeg.degrad;

			HOAEncoder.ar(
				order,
				outsig,
				CircleRamp.kr(az, slewPanning, -1pi, 1pi),
				CircleRamp.kr(el, slewPanning, -1pi, 1pi),
				0, // gain
				plane_spherical,
				radius,
				speaker_radius
			)
		}
	}

	*makeSynthDef {
		|
		name,
		src,
		preBusArray,
		postBusArray,
		preMonitorArray,				// pre-bus monitoring/analysis functions
		spatializerArray,
		firstOutputIndexArray,
		paramsMapper,
		synthArgs
		|

		SynthDef(name,
			{| synth_gate = 1, preBus_gainDB = 0, postBus_gainDB = 0  |
				var in, env, sidechain, out, mapped;
				// install first the mapper with spatialization parameters, allowing it to take control
				// over all defined parameter
				mapped = SynthDef.wrap(paramsMapper);
				// in
				in = SynthDef.wrap(src, prependArgs:  synthArgs);
				// fade in set to as short as possible for percussive cases
				env = EnvGen.kr(Env.cutoff(0.01, 1, 2),  synth_gate, doneAction: Done.freeSelf);
				// open a side-chain. Side-chain consumes an audio input and does analysis on it.
				preMonitorArray.do {arg item, i;
					SynthDef.wrap(item, prependArgs: [in*env]);
				};

				// in -> busses (busses are taking raw input)
				preBusArray.do {arg item;
					Out.ar(item, preBus_gainDB.dbamp * env * in);
				};
				// in -> dest
				// collecting spatializers
				out = Array.newClear(spatializerArray.size());
				spatializerArray.do { arg item, i;
					out.put(i, env * SynthDef.wrap(item, prependArgs: [in] ++ mapped.at(i.mod(mapped.size))));
				};
				// sending sum of first spatializer to the post busses
				postBusArray.do { arg item;
					Out.ar(item, postBus_gainDB.dbamp * env * Mix.new(out.at(0)));
				};
				// sending to out

				spatializerArray.do { arg item, i;
					Out.ar(firstOutputIndexArray.wrapAt(i), out.at(i));
				}
		}).add(Satie.synthDescLibName);

		SynthDef(name ++ "_kamikaze",
			{ | synth_gate = 1, preBus_gainDB = 0, postBus_gainDB = 0  |
				var in, env, out, mapped;
				// install first the mapper with spatialization parameters, allowing it to take control
				// over all defined parameter
				mapped = SynthDef.wrap(paramsMapper);
				// in
				in = SynthDef.wrap(src, prependArgs:  synthArgs);
				DetectSilence.ar(in, doneAction: Done.freeSelf);
				// fade in set to as short as possible for percussive cases
				env = EnvGen.kr(Env.cutoff(0.01, 1, 2),  synth_gate, doneAction: Done.freeSelf);
				// open a side-chain. Side-chain consumes an audio input and does analysis on it.
				preMonitorArray.do {arg item, i;
					SynthDef.wrap(item, prependArgs: [in*env]);
				};
				// in -> busses (busses are taking raw input)
				preBusArray.do {arg item;
					Out.ar(item, preBus_gainDB.dbamp * env * in);
				};
				// in -> dest
				// collecting spatializers
				out = Array.newClear(spatializerArray.size());
				spatializerArray.do { arg item, i;
					out.put(i, env * SynthDef.wrap(item, prependArgs: [in] ++ mapped.at(i.mod(mapped.size))));
				};
				// sending sum of first spatializer to the post busses
				postBusArray.do { arg item;
					Out.ar(item, postBus_gainDB.dbamp * env * Mix.new(out.at(0)));
				};
				// sending to out
				spatializerArray.do { arg item, i;
					Out.ar(firstOutputIndexArray.wrapAt(i), out.at(i));
				}
		}).add(Satie.synthDescLibName);
	}

	*makeAmbiFromMono {
		|
		name,
		src,
		preBusArray,
		postBusArray,
		preMonitorArray,
		hoaEncoderType,
		ambiOrder,
		ambiEffectPipeline,
		ambiBus,
		paramsMapper,
		synthArgs
		|

		SynthDef(name,
			{| synth_gate = 1, preBus_gainDB = 0, postBus_gainDB = 0  |
				var in, env, out, mapped, encoder;
				// install first the mapper with spatialization parameters, allowing it to take control
				// over all defined parameter
				mapped = SynthDef.wrap(paramsMapper);
				// in
				in = SynthDef.wrap(src, prependArgs:  synthArgs);
				// fade in set to as short as possible for percussive cases
				env = EnvGen.kr(Env.cutoff(0.01, 1, 2),  synth_gate, doneAction: Done.freeSelf);
				// open a side-chain. Side-chain consumes an audio input and does analysis on it.
				preMonitorArray.do {arg item, i;
					SynthDef.wrap(item, prependArgs: [in*env]);
				};
				// in -> busses (busses are taking raw input)
				preBusArray.do {arg item;
					Out.ar(item, preBus_gainDB.dbamp * env * in);
				};
				// encoder part
				encoder = SynthDef.wrap(this.encoderUGenGraph(hoaEncoderType), prependArgs: [in, ambiOrder] ++ mapped.at(0));

				out = env * encoder;
				postBusArray.do { arg item;
					Out.ar(item, postBus_gainDB.dbamp * env * NumChannels.ar(out,numChannels: 1, mixdown: false));
				};
				// sending to out
				Out.ar(ambiBus, out);
		}).add(Satie.synthDescLibName);

		SynthDef(name ++ "_kamikaze",
			{| synth_gate = 1, preBus_gainDB = 0, postBus_gainDB = 0  |
				var in, env, out, mapped, encoder;
				// install first the mapper with spatialization parameters, allowing it to take control
				// over all defined parameter
				mapped = SynthDef.wrap(paramsMapper);
				// in
				in = SynthDef.wrap(src, prependArgs:  synthArgs);
				DetectSilence.ar(in, doneAction: Done.freeSelf);
				// fade in set to as short as possible for percussive cases
				env = EnvGen.kr(Env.cutoff(0.01, 1, 2),  synth_gate, doneAction: Done.freeSelf);
				// open a side-chain. Side-chain consumes an audio input and does analysis on it.
				preMonitorArray.do {arg item, i;
					SynthDef.wrap(item, prependArgs: [in*env]);
				};
				// in -> busses (busses are taking raw input)
				preBusArray.do {arg item;
					Out.ar(item, preBus_gainDB.dbamp * env * in);
				};
				// encoder part
				encoder = SynthDef.wrap(this.encoderUGenGraph(hoaEncoderType), prependArgs: [in, ambiOrder] ++ mapped.at(0));

				out = env * encoder;
				postBusArray.do { arg item;
					Out.ar(item, postBus_gainDB.dbamp * env * NumChannels.ar(out,numChannels: 1, mixdown: false));
				};
				// sending to out
				Out.ar(ambiBus, out);
		}).add(Satie.synthDescLibName);

	}

	// make Ambi is dedicated to src function with channelLayout set to \ambi
	*makeAmbi {
		|
		name,
		src,
		preBusArray,
		postBusArray,
		preMonitorArray,
		ambiOrder,
		ambiEffectPipeline,
		ambiBus,
		paramsMapper,
		synthArgs
		|

		SynthDef(name,
			{| synth_gate = 1, preBus_gainDB = 0, postBus_gainDB = 0  |
				var in, env, out, mapped, encoder;
				// install first the mapper with spatialization parameters, allowing it to take control
				// over all defined parameter
				mapped = SynthDef.wrap(paramsMapper);
				// in
				in = SynthDef.wrap(src, prependArgs:  [ambiOrder] ++ synthArgs);
				// fade in set to as short as possible for percussive cases
				env = EnvGen.kr(Env.cutoff(0.01, 1, 2),  synth_gate, doneAction: Done.freeSelf);
				// open a side-chain. Side-chain consumes an audio input and does analysis on it.
				preMonitorArray.do {arg item, i;
					SynthDef.wrap(item, prependArgs: [in*env]);
				};
				// in -> busses (busses are taking raw input)
				preBusArray.do {arg item;
					Out.ar(item, preBus_gainDB.dbamp * env * in);
				};
				out = env * in;
				postBusArray.do { arg item;
					Out.ar(item, postBus_gainDB.dbamp * env * NumChannels.ar(out,numChannels: 1, mixdown: false));
				};
				// sending to out
				Out.ar(ambiBus, out);
		}).add(Satie.synthDescLibName);

		SynthDef(name ++ "_kamikaze",
			{| synth_gate = 1, preBus_gainDB = 0, postBus_gainDB = 0  |
				var in, env, out, mapped, encoder;
				// install first the mapper with spatialization parameters, allowing it to take control
				// over all defined parameter
				mapped = SynthDef.wrap(paramsMapper);
				// in
				in = SynthDef.wrap(src, prependArgs: [ambiOrder] ++ synthArgs);
				DetectSilence.ar(in, doneAction: Done.freeSelf);
				// fade in set to as short as possible for percussive cases
				env = EnvGen.kr(Env.cutoff(0.01, 1, 2),  synth_gate, doneAction: Done.freeSelf);
				// open a side-chain. Side-chain consumes an audio input and does analysis on it.
				preMonitorArray.do {arg item, i;
					SynthDef.wrap(item, prependArgs: [in*env]);
				};
				// in -> busses (busses are taking raw input)
				preBusArray.do {arg item;
					Out.ar(item, preBus_gainDB.dbamp * env * in);
				};
				out = env * in;
				postBusArray.do { arg item;
					Out.ar(item, postBus_gainDB.dbamp * env * NumChannels.ar(out,numChannels: 1, mixdown: false));
				};
				// sending to out
				Out.ar(ambiBus, out);
		}).add(Satie.synthDescLibName);


	}
}
