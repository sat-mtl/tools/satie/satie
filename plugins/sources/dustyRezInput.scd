// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

/*  Audio plugin definition

	Each audio plugin should define the following global variables:
	~name: (symbol) name of the spatializer
	~description: (string) a short description
	~function: the actual definition of the plugin
*/

~name = \dustyRezInput;
~description = "Dust through resonators driven by live input";
~channelLayout = \mono;

~function = {
	|bus = 0, t_trig = 0, in_fact = 0.5, density = 10, attack=100, dec = 0.3, sus = 0.5, rel = 6, amp = 0.75, gate = 1|
	var input, freq, hasFreq, ampCtl, env, envGen, dust, resonance, verb;
	input = SoundIn.ar(bus, EnvGen.kr(Env([0,1], [1,1]), t_trig));
	# freq , hasFreq = Pitch.kr(input, ampThreshold: 0.02, median: 7);
	freq = VarLag.kr(freq, time: 2, warp: 7);
	ampCtl = Lag.kr(PeakFollower.kr(input));
	env = Env.adsr(attack, dec, sus, rel);
	envGen = EnvGen.kr(env, gate);
	dust = Dust.ar(density*ampCtl);
	Select.kr(
		hasFreq > 0,
		[
			resonance = DynKlank.ar(`[
				// frequency ratios
				[0.501, 1, 0.7,   2.002, 3, 9.6,   2.49, 11, 2.571,  3.05, 6.242, 12.49, 13, 16, 24],
				// amps
				[0.002,0.02,0.001, 0.008,0.02,0.004, 0.02,0.04,0.02, 0.005,0.05,0.05, 0.02, 0.03, 0.04],
				// ring times - "stutter" duplicates each entry threefold
				[5.2, 3.9, 2.25, 2.14, 1.07].stutter(3)
			],
				dust*ampCtl*in_fact, freq), Silent.ar()]
	);
	verb = FreeVerb.ar(resonance*ampCtl, ampCtl, 1, 0.5, Line.kr(0, 0.6, 0.5, mul: 0.7));
	verb = LeakDC.ar(verb);
	verb * envGen * amp;
};

~setup = {};
