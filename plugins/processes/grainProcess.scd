// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

~grainProcess = Environment.make({ |self|

	~name;
	~group;
	~routine;

	~interval = 0.5;
	~attMin = 0.001;
	~attMax = 0.05;
	~relMin = 0.001;
	~relMax = 0.02;
	~startPos = 0.0;
	~durMin = 0.001;
	~durMax = 0.03;
	~rateMin = 0.94;
	~rateMax = 1.03;
	~trimDB = -10;
	~aziDeg = 0;
	~eleDeg = 0;
	~bufnum;

	~makeRoutine = { |self|
		self.routine = Routine {
			loop {
				self.satieInstance.makeKamikaze(
					synthDefName: \grainBufASR,
					group: self.group,
					synthArgs: [
						\bufnum, self.bufnum,
						\rate, rrand(self.rateMin, self.rateMax),
						\startPos, self.startPos + rand(1.0) - self.durMax,
						\gainDB, rrand(-25, -18),
						\trimDB, self.trimDB,
						\att, rrand(self.attMin, self.attMax),
						\rel, rrand(self.relMin, self.relMax),
						\dur, rrand(self.durMin, self.durMax),
						\aziDeg, (self.aziDeg + rand2(5.0)).wrap2(180),
						\eleDeg, (self.eleDeg + rand2(5.0)).clip2(90),
					]
				);
				self.interval.yield;
			}
		}
	};

	~start = { |self|
		self.routine.reset;
		self.routine.play;
	};

	~setup = { |self, nodeName, groupName|
		self.name = nodeName.asSymbol;
		self.group = groupName.asSymbol;
		self.makeRoutine;
	};

	~cleanup = { |self|
		self.routine.stop;
	};

});

