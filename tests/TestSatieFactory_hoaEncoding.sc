TestSatieFactory_hoaEncoding : SatieUnitTest {

	var server, config, satie;

	setUp {
		server = Server(this.class.name);
		config = SatieConfiguration(server, ambiOrders: [3]);

		// avoid "exceeded number of interconnect buffers" errors when compiling plugins
		server.options.numWireBufs = 1024;
	}

	tearDown {
		this.quit(satie);
		server.remove;
	}

	test_hoaEncoderType_wave {
		var ugens;

		satie = Satie(config);
		satie.config.hoaEncoderType_(\wave);
		this.boot(satie);

		ugens = satie.synthDescLib.synthDescs[\testtoneAmbi3].def.children.collect { |ugen| ugen.class.name.asSymbol };
		this.assert(ugens.includes(\HOAEncoder3), "SatieFactory compiled SynthDef using \\wave type HOA encoding")
	}

	test_hoaEncoderType_harmonic {
		var ugens;

		satie = Satie(config);
		satie.config.hoaEncoderType_(\harmonic);
		this.boot(satie);

		ugens = satie.synthDescLib.synthDescs[\testtoneAmbi3].def.children.collect { |ugen| ugen.class.name.asSymbol };

		// we can't test for the inclusion of HOASphericalHarmonics
		// When compiled into a SynthDef, this pseudo-UGen gets disolved into a bunch of BinaryOpUGens
		// all we can do is test that HOAEncoder is not used inside this SynthDef
		this.assertEquals(ugens.includes(\HOAEncoder3), false, "SatieFactory compiled SynthDef using \\harmonic type HOA encoding")
	}

	test_hoaEncoderType_lebedev50 {
		var ugens;

		satie = Satie(config);
		satie.config.hoaEncoderType_(\lebedev50);
		this.boot(satie);

		ugens = satie.synthDescLib.synthDescs[\testtoneAmbi3].def.children.collect { |ugen| ugen.class.name.asSymbol };
		this.assertEquals(ugens.includes(\HOAEncLebedev503), true, "SatieFactory compiled SynthDef using \\lebedev50 type HOA encoding")
	}

}

