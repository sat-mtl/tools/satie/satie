SatieUnitTest : UnitTest {

	classvar <>debugTests = false;
	classvar <allSubclasses;

	*initClass {
		allSubclasses = this.subclasses.collect { |test| test.name.asClass };
	}

	*new {
		^super.new;
	}

	// implements *runAll for SatieUnitTest
	// this is similar to UnitTest.runAll but only runs Satie's tests
	*runAll {
		^this.forkIfNeeded {
			this.reset;
			this.allSubclasses.do { |testClass|
				testClass.run(false, false);
				1.wait;
			};
			this.report;
		}
	}

	wait { |condition, failureMessage = "Wait condition failed", maxTime = 30|
		var limit = maxTime;

		while { (condition.value.not) && (limit >= 0) } {
			1.wait;
			limit = limit - 1;
			if(debugTests) {
				"% waited for % seconds".format(thisMethod, (maxTime - limit)).warn
			};
		};

		if(limit < 0) {
			this.failed(thisMethod, failureMessage);
		};
	}

	boot { |satie|
		satie.boot;
		this.wait(
			condition: { satie.status === \running },
			failureMessage: "%: Satie failed to boot".format(thisMethod)
		);

		if(debugTests) {
			"%: Server.serverRunning is: %, Satie status is: %"
				.format(thisMethod, satie.config.server.serverRunning, satie.status).warn;
		};
	}

	quit { |satie|
		satie.quit;
		this.wait(
			condition: { satie.status === \initialized },
			failureMessage: "% Satie failed to quit Server".format(thisMethod),
			maxTime: 3
		);

		if(debugTests) {
			"%: Server.serverRunning is: %, Satie status is: %"
				.format(thisMethod, satie.config.server.serverRunning, satie.status).warn;
		};
	}

}
